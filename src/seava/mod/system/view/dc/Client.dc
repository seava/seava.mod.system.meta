import global.base.*

import seava.mod.system.*
import seava.mod.system.presenter.*
import seava.mod.system.lov.*

package seava.mod.system.dc module: system {

  data-control Client < Client > {

    form-view for-filter FilterV {
      text-field code : code
        lov: Clients returns ( id : id ) ;
      text-field name : name
        lov: ClientNames returns ( id : id ) ;
      boolean-field active : active ;

      panel FORM main [ code , name , active ]
        children-defaults: { labelWidth : "60" } ;
    }

    /* ----------------------------------------------- */

    grid-view List read-only {
      text-field code : code 120 ;
      text-field name : name 200 ;
      text-field description : description 200 hidden ;
      text-field notes : notes 200 hidden ;
      text-field adminRole : adminRole ;
      boolean-field active : active ;

      text-field workspacePath : workspacePath 250 ;
      text-field importPath : importPath 250 hidden ;
      text-field exportPath : exportPath 250 hidden ;
      text-field tempPath : tempPath 250 hidden ;
    }

    /* ----------------------------------------------- */

    form-view Create {
      text-field name : name not-null ;
      text-field code : code not-null ;

      text-field description : description , 60 multi-line ;
      text-field notes : notes , 60 multi-line ;

      text-field adminUserCode : adminUserCode "Code" not-null ;
      text-field adminUserName : adminUserName "Name" not-null ;
      text-field adminUserLogin : adminUserLogin "Login" not-null ;
      text-field adminPassword : adminPassword "Password" not-null
        attributes: { inputType : "password" } ;

      text-field initFileLocation : initFileLocation ;
      text-field workspacePath : workspacePath not-null
        events: change createPathsFromWorkspace ;
      text-field importPath : importPath not-null ;
      text-field exportPath : exportPath not-null ;
      text-field tempPath : tempPath not-null ;

      panel HORIZONTAL main [ col1 , col2 , col3 ] ;

      panel FORM col1 [ name , code , description , notes ] width: 400 ;

      panel FORM col2 [ adminUserCode , adminUserName , adminUserLogin , adminPassword ]
        "Administrator user" width: 300
        attributes: { xtype : "fieldset" , collapsible : "true" , border : "true" , margin_txt :
        "0 0 0 5" } ;

      panel FORM col3 [ initFileLocation , workspacePath , importPath , exportPath , tempPath ]
        "Working directories" width: 550
        attributes: { xtype : "fieldset" , collapsible : "true" , border : "true" , margin_txt :
        "0 0 0 5" } ;

      function createPathsFromWorkspace ( field , nv , ov ) '
        var r = this._controller_.record;
        if (nv) {
          r.set("importPath", nv + "/import");
          r.set("exportPath", nv + "/export");
          r.set("tempPath", nv + "/temp");
        }        
'
    }

    /* ----------------------------------------------- */

    form-view Edit {
      text-field name : name not-null ;
      text-field code : code not-null ;
      text-field adminRole : adminRole no-edit ;
      boolean-field active : active ;

      text-field description : description , 60 multi-line ;
      text-field notes : notes , 60 multi-line ;

      text-field workspacePath : workspacePath not-null ;
      text-field importPath : importPath not-null ;
      text-field exportPath : exportPath not-null ;
      text-field tempPath : tempPath not-null ;

      panel HORIZONTAL main [ col1 , col2 ] ;

      panel FORM col1 [ name , code , description , notes , adminRole , active ] width: 400 ;
      panel FORM col2 [ workspacePath , importPath , exportPath , tempPath ] "Working directories"
        width: 550
        attributes: { xtype : "fieldset" , collapsible : "true" , border : "true" , margin_txt :
        "0 0 0 5" } ;
    }
  }
}
	